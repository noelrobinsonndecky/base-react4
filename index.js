// Les formulaires au sein de React

// Je crée ma class principale
//exple
class Principal extends React.Component{


    // Notre constructeur
    constructor(props){
        super(props)
        this.state={
            nom: "jean",
            message: "message",
            select: "expl1",
            checked: true
        }
        this.handleChange= this.handleChange.bind(this)
        this.handleChange1= this.handleChange1.bind(this)
        this.handleChange2= this.handleChange2.bind(this)
        this.handleChange3= this.handleChange3.bind(this)
    }

    handleChange (e) {
        this.setState({
            nom: e.target.value
        })
    }

    handleChange1 (e) {
        this.setState({
            message: e.target.value
        })
    }

    handleChange2 (e) {
        this.setState({
            select: e.target.value
        })
    }

    handleChange3 (e) {
        this.setState({
            checked: e.target.checked
        })
    }

    render() {
        return (
            // en react, l'utilisation de for dans les label s'écrit (htmlFor)
            // onChange permet de gérer le changement des événements 
            <div className="div1">
                
                <label htmlFor="name">Mon nom:</label> <br/>
                <input className="ipt"
                    type="text" id="name" name="nom" value={this.state.nom} 
                    onChange={this.handleChange} 
                />

                <br/>
                
                <label htmlFor="name">Mon message:</label> <br/>
                <textarea className="ipt" id="message" name="message" value={this.state.message} 
                    onChange={this.handleChange1} rows="" cols="">
                </textarea>

                <br/>
                
                <p>Vous avez choisi: {this.state.select} </p>
                <label htmlFor="select">Choisir:</label> <br/>
                <select id="select" value={this.state.select} onChange={this.handleChange2}>
                    <option value="exple1">exple1</option>
                    <option value="exple2">exple2</option>
                    <option value="exple3">exple3</option>
                    <option value="exple4">exple4</option>
                    <option value="exple5">exple5</option>
                </select>

                <br/>

                <label htmlFor="checked">Merci de cocher cette case</label>
                <input type="checkbox" checked={this.state.checked} onChange={this.handleChange3}/>

                {this.state.checked ? <p>Merci d'avoir cocher cette case</p> : null}
            </div>
        )
    }
}


/*

// Un simple formulaire

class Formulaire extends React.Component{

    // mon constructeur
    constructor(props){
        super(props)
        this.state= {
            nom: '',
            prenom: '',
            newsLetter: false
        }
        this.handleChange= this.handleChange.bind(this)

    }

    handleChange(e){

        // On récupére le nom du champ à modifier
        const name=e.target.name
        const type= e.target.type
        const value= type === 'checkbox' ? e.target.checked : e.target.value
        this.setState({
            [name]: value
        })
    }

    render() {
        return (
            <div id="div2" className="div2">
                <div>
                    <label htmlFor="nom">Nom: </label>
                    <input id="nom" type="text" name="nom" value={this.state.nom} onChange={this.handleChange}/>
                </div>
                <br/>
                <div>
                    <label htmlFor="Prénom">Prénom: </label>
                    <input id="prenom" type="text" name="prenom" value={this.state.prenom} onChange={this.handleChange}/>
                </div>
                <br/>
                <div>
                    <label htmlFor="NewLetter">NewsLetter: </label>
                    <input type="checkbox" checked={this.state.checked} onChange={this.handleChange}/>
                </div>
                {JSON.stringify(this.state)}
            </div>
        )
    }
}

*/
function PagePrincipale(){
    return (
        <div>
            <Principal/>
            
        </div>
    )
}

ReactDOM.render(
        <PagePrincipale/>,
    document.querySelector('#app')
)