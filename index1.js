
// Ma fonction pour mes Inputs
function Field ({onChange, value, name, children}) {

    return (
        <div className="form-group">
            <label htmlFor={name}>{children}</label>
            <input type="text" name={name} value={value} onChange={onChange} id={name} className="form-control"/>
        </div>
    )
}

// Ma fonction pour mon checkbox
function Checkbox ({onChange, value, name, children}) {

    return (
        <div className="form-check">
            <input type="checkbox" name={name} value={value} onChange={onChange} id={name} className="form-control"/>
            <label htmlFor={name} className="form-check-label">{children}</label>
        </div>
    )
}


// Un formulaire avec bootstrap

class Formulaire extends React.Component{

    // mon constructeur
    constructor(props){
        super(props)
        this.state= {
            nom: '',
            prenom: '',
            newsLetter: false
        }
        this.handleChange= this.handleChange.bind(this)
        this.handleSubmit= this.handleSubmit.bind(this)

    }

    handleChange(e){

        // On récupére le nom du champ à modifier
        const name=e.target.name
        const type= e.target.type
        const value= type === 'checkbox' ? e.target.checked : e.target.value
        this.setState({
            [name]: value
        })
    }

    handleSubmit (e){
        e.preventDefault()
        const data= JSON.stringify(this.state)
        console.log(data)
        this.setState({
            nom: '',
            prenom: '',
        })
    }

    render() {
        return (
            <form className="container" onSubmit={this.handleSubmit}>

                <Field name="nom" value={this.state.name} onChange={this.handleChange}>Nom: </Field>
                <Field name="prenom" value={this.state.prenom} onChange={this.handleChange}>Prenom: </Field>
                <Checkbox name="NewsLetter" checked={this.state.newsLetter} onChange={this.handleChange}>
                    Vous abonnez à la newsLetter ?
                </Checkbox>
                <div className="form-group">
                    <button className="btn btn-primary">Envoyer</button>
                </div>
                {JSON.stringify(this.state)}
            </form>
        )
    }
}

function Principal (){
    return (
        <div>
            <Formulaire/>
        </div>
    )
}

ReactDOM.render(
    <Principal/>,
    document.querySelector('#app1')
)